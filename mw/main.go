package mw

import (
	"apilaundry/models"
	"fmt"

	"strings"

	"github.com/gin-gonic/gin"
)

func AuthRequired(mu models.Userservice) gin.HandlerFunc {
	return func(c *gin.Context) {
		header := c.GetHeader("Authorization")
		token := strings.TrimSpace(header[len("Bearer "):])
		if len(token) <= 7 {
			c.Status(401)
			c.Abort()
			return
		}
		if token == "" {
			c.Status(401)
			c.Abort()
			return
		}

		user, err := mu.GetUserByToken(token)
		if err != nil {
			c.Status(401)
			c.Abort()
			return
		}
		_ = user
		c.Set("user", user)

		userLogin := c.Value("user").(*models.User)
		fmt.Printf("Check data User : %v", userLogin)
	}
}

func AuthRequiredPartner(mp models.Partnerservice) gin.HandlerFunc {
	return func(c *gin.Context) {
		header := c.GetHeader("Authorization")
		tokenp := strings.TrimSpace(header[len("Bearer "):])
		if len(tokenp) <= 7 {
			c.Status(401)
			c.Abort()
			return
		}
		if tokenp == "" {
			c.Status(401)
			c.Abort()
			return
		}
		partner, err := mp.GetPartnerByToken(tokenp)
		if err != nil {
			c.Status(401)
			c.Abort()
			return
		}

		_ = partner
		c.Set("partner", partner)
		// partnerLogin := c.Value("partner").(models.Partner)
		// fmt.Printf("Check data User : %v", partnerLogin)
	}

}
