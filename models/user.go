package models

import (
	"apilaundry/rand"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"hash"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

const key = 12
const hmacKey = ""

type User struct {
	gorm.Model
	Username string `gorm:"not null" json:"name`
	Email    string `gorm:"unique_index;not null" json:"email"`
	Password string `gorm:"not null" json:"password"`
	Token    string `gorm:"unique_index"`
	Phone    string
	Dob      string
	Address  string
	Filename string `gorm:"not null "`
}

type Userservice interface {
	UserLoginpost(user *User) (string, error)
	GetUserByToken(token string) (*User, error)
	UserSignup(user *User) error
	UserLogOut(id uint) error
	GetProfile(userid uint) (*User, error)
}

type UserGorm struct {
	db   *gorm.DB
	hmac hash.Hash
}

func NewUserService(db *gorm.DB, key string) Userservice {
	mac := hmac.New(sha256.New, []byte(key))
	return &UserGorm{db, mac}
}

func (mu *UserGorm) UserLoginpost(user *User) (string, error) {
	found := new(User)
	err := mu.db.Where("email=? ", user.Email).First(&found).Error
	if err != nil {
		return "", errors.New("not found email!")
	}
	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(user.Password))
	if err != nil {
		return "", errors.New("password is wrong!")
	}

	token, err := rand.GetToken()
	if err != nil {
		return "", err
	}

	mu.hmac.Write([]byte(token))
	newToken := mu.hmac.Sum(nil)
	mu.hmac.Reset()

	err = mu.db.Model(&User{}).Where("id=?", found.ID).Update("token", base64.URLEncoding.EncodeToString(newToken)).Error
	if err != nil {
		return "", err
	}
	return token, nil
}

func (mu *UserGorm) UserSignup(user *User) error {
	const cost = 12
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), cost)
	if err != nil {
		return err
	}

	user.Password = string(hash)
	token, err := rand.GetToken()
	if err != nil {
		return err
	}
	user.Token = token
	return mu.db.Create(user).Error
}

func (mu *UserGorm) GetUserByToken(token string) (*User, error) {
	user := new(User)
	mu.hmac.Write([]byte(token))
	newToken := mu.hmac.Sum(nil)
	mu.hmac.Reset()
	err := mu.db.Where("token = ?", base64.URLEncoding.EncodeToString(newToken)).First(user).Error
	if err != nil {
		return nil, err
	}
	fmt.Printf("Userid Login %v", user)
	return user, nil
}

func (mu *UserGorm) UserLogOut(id uint) error {
	token, err := rand.GetToken()
	if err != nil {
		return err
	}
	mu.hmac.Write([]byte(token))
	newToken := mu.hmac.Sum(nil)
	mu.hmac.Reset()

	err = mu.db.Model(&User{}).Where("id=?", id).Update("token", base64.URLEncoding.EncodeToString(newToken)).Error
	if err != nil {
		return err
	}
	return err
}

func (mu *UserGorm) GetProfile(userid uint) (*User, error) {
	user := new(User)
	if err := mu.db.Where("id=?", userid).First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}
