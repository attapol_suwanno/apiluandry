package models

import (
	"apilaundry/rand"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"hash"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type Partner struct {
	gorm.Model
	Partnername string `gorm:"not null" json:"partnername`
	Email       string `gorm:"unique_index;not null" json:"email"`
	Password    string `gorm:"not null" json:"password"`
	Token       string `gorm:"unique_index"`
	Tall        string
	Filename    string
}

type Partnerservice interface {
	PartnerLoginpost(partner *Partner) (string, error)
	PartnerSignup(partner *Partner) error
	GetPartnerByToken(token string) (*Partner, error)
	UserPartner(partnerid uint) error
}

type PartnerGorm struct {
	db   *gorm.DB
	hmac hash.Hash
}

func NewPartnerService(db *gorm.DB, key string) Partnerservice {
	mac := hmac.New(sha256.New, []byte(key))
	return &PartnerGorm{db, mac}
}

func (mp *PartnerGorm) PartnerLoginpost(partner *Partner) (string, error) {
	found := new(Partner)
	err := mp.db.Where("email=? ", partner.Email).First(&found).Error
	if err != nil {
		return "", errors.New("not found email!")
	}
	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(partner.Password))
	if err != nil {
		return "", errors.New("password is wrong!")
	}

	token, err := rand.GetToken()
	if err != nil {
		return "", err
	}

	mp.hmac.Write([]byte(token))
	newToken := mp.hmac.Sum(nil)
	mp.hmac.Reset()

	err = mp.db.Model(&Partner{}).Where("id=?", found.ID).Update("token", base64.URLEncoding.EncodeToString(newToken)).Error
	if err != nil {
		return "", err
	}
	return token, nil
}

func (mp *PartnerGorm) PartnerSignup(partner *Partner) error {
	const cost = 12
	hash, err := bcrypt.GenerateFromPassword([]byte(partner.Password), cost)
	if err != nil {
		return err
	}

	partner.Password = string(hash)
	token, err := rand.GetToken()
	if err != nil {
		return err
	}
	partner.Token = token
	return mp.db.Create(partner).Error
}

func (mp *PartnerGorm) GetPartnerByToken(token string) (*Partner, error) {
	partner := new(Partner)
	mp.hmac.Write([]byte(token))
	newToken := mp.hmac.Sum(nil)
	mp.hmac.Reset()
	err := mp.db.Where("token = ?", base64.URLEncoding.EncodeToString(newToken)).First(partner).Error
	if err != nil {
		return nil, err
	}
	fmt.Printf("Userid Login %v", partner)
	return partner, nil
}

func (mp *PartnerGorm) UserPartner(partnerid uint) error {
	token, err := rand.GetToken()
	if err != nil {
		return err
	}
	mp.hmac.Write([]byte(token))
	newToken := mp.hmac.Sum(nil)
	mp.hmac.Reset()

	err = mp.db.Model(&Partner{}).Where("id=?", partnerid).Update("token", base64.URLEncoding.EncodeToString(newToken)).Error
	if err != nil {
		return err
	}
	return err
}
