package models

import (
	"github.com/jinzhu/gorm"
)

type Store struct {
	gorm.Model
	Storename      string      `gorm:"not null" json:"storename`
	Prices         []*Price    `json:"prices" gorm:"prices;one2many;prices;foreignkey:store_id;association_foreignkey:id"`
	Location       []*Location `json :"location" gorm:"location;one2many;location;foreignkey:store_id;association_foreignkey:id"`
	Dry            int
	Fabricsof      int
	Detergent      int
	Promotion      string
	PromotionPrice int
	Filename       string
}

type StoreModel struct {
	ID             uint
	Storename      string `gorm:"not null" json:"storename`
	Prices         []*Price
	Location       []*Location
	Dry            int
	Fabricsof      int
	Detergent      int
	Promotion      string
	PromotionPrice int
	Filename       string
}

type Price struct {
	gorm.Model
	StoreID     uint `json:"store_id" gorm:"column:store_id"`
	Pricesmall  float32
	PriceMiddle float32
	PriceBig    float32
}
type Location struct {
	gorm.Model
	StoreID   uint `json:"store_id" gorm:"column:store_id"`
	Latitude  float32
	Longitude float32
}

type Storeservice interface {
	ListStoreAll(searchInput string) ([]Store, error)
	GetStoreID(storeid uint) (*Store, error)
	GetLocation(storeid uint) (*Location, error)
	GetNameStore(search string) ([]Store, error)
}

type StoreGorm struct {
	db *gorm.DB
}

func NewStoreService(db *gorm.DB) Storeservice {
	return &StoreGorm{db}
}

func (ms *StoreGorm) ListStoreAll(searchInput string) ([]Store, error) {
	stores := []Store{}
	if err := ms.db.Preload("Prices").Preload("Location").Find(&stores).Error; err != nil {
		return nil, err
	}
	if searchInput != "" {
		if err := ms.db.Preload("Prices").Preload("Location").Where("storename LIKE ?", "%"+searchInput+"%").Find(&stores).Error; err != nil {
			return nil, err
		}
	}
	return stores, nil
}

func (ms *StoreGorm) GetStoreID(storeid uint) (*Store, error) {
	stores := Store{}
	if err := ms.db.Preload("Prices").Preload("Location").Where("id=?", storeid).First(&stores).Error; err != nil {
		return nil, err
	}
	return &stores, nil
}

func (ms *StoreGorm) GetLocation(storeid uint) (*Location, error) {
	stores := Location{}
	if err := ms.db.Where("store_id=?", storeid).First(&stores).Error; err != nil {
		return nil, err
	}
	return &stores, nil
}

func (ms *StoreGorm) GetNameStore(search string) ([]Store, error) {
	stores := []Store{}
	if err := ms.db.Where("storename=?", search).Find(&stores).Error; err != nil {
		return nil, err
	}
	return stores, nil
}
