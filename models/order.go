package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type Orders struct {
	gorm.Model
	Userid    uint
	Storeid   uint
	Partnerid uint
	Price     float32
	Dry       int
	Detergent int
	Fabricsof int
	Status    int
}

type OrdersModel struct {
	ID        uint
	Userid    uint
	Storeid   uint
	Partnerid uint
	Price     float32
	Dry       int
	Detergent int
	Fabricsof int
	Status    int
}

type ReportOrder struct {
	ID        uint
	Userid    uint
	Storeid   uint
	Partnerid uint
	Price     float32
	Dry       int
	Detergent int
	Fabricsof int
	Status    int
	Storename string
}

type Ordersservice interface {
	CreateOrdertable(orders *Orders) error
	GetOrderByID(orderid uint) (*OrderDetailByUser, error)
	GetOrderByUser(userid uint) ([]ListOrderUser, error)
	GetOrderDetailByID(orderid uint) (*ListOrderUser, error)
	ListHistoryAll(userid uint) ([]ListOrderUser, error)
	GetOrderActive() ([]OrderDetailByUser, error)
	UpdateStatusTwo(order *Orders) error
	GetOrderByPartner(partnerid uint) ([]ListOrderUser, error)
	GetPartnerOrder(orderid uint) (*DetailOrderWith, error)
	ListPartnerHistoryAll(id uint) ([]ListOrderUser, error)
	GetorderPartnerByID(orderid uint) (*Orders, error)
	DeleteOrder(id uint) error
	UpdateStatusFinish(orderid uint) error
	GetorderDetailPartnerByID(orderid uint) (*DetailOrderWith, error)
}
type OrdersGorm struct {
	db *gorm.DB
}

func NewOrdersService(db *gorm.DB) Ordersservice {
	return &OrdersGorm{db}
}

func (mo *OrdersGorm) CreateOrdertable(orders *Orders) error {
	return mo.db.Create(orders).Error
}

type OrderDetailByUser struct {
	Ordersid  uint
	Userid    uint
	Storeid   uint
	Partnerid uint
	Price     float32
	Dry       int
	Detergent int
	Fabricsof int
	Status    int
	Storename string
	Filename  string
}

func (mo *OrdersGorm) GetOrderByID(orderid uint) (*OrderDetailByUser, error) {
	rows, err := mo.db.Table("orders").
		Select("orders.id, orders.userid, orders.storeid ,orders.partnerid ,orders.price ,orders.dry ,orders.detergent,orders.fabricsof ,orders.status,stores.storename,stores.filename").
		Joins("join stores on stores.id = orders.storeid").
		Where("orders.id = ?", orderid).Rows()
	if err != nil {
		fmt.Println(err)
	}
	report := OrderDetailByUser{}
	for rows.Next() {
		err := rows.Scan(&report.Ordersid, &report.Userid, &report.Storeid, &report.Partnerid, &report.Price, &report.Dry, &report.Detergent, &report.Fabricsof, &report.Status, &report.Storename, &report.Filename)
		if err != nil {
			fmt.Println(err)
		}
	}
	if err != nil {
		return nil, err
	}
	return &report, nil
}

func (mo *OrdersGorm) DeleteOrder(id uint) error {
	data := new(Orders)
	if err := mo.db.Where("id=?", id).First(data).Error; err != nil {
		return err
	}
	return mo.db.Delete(data).Error
}

type ListOrderUser struct {
	Ordersid    uint
	Storename   string
	Status      int
	Filename    string
	Partnername string
	Tall        string
	Partnerid   string
	Price       float32
	Dry         int
	Detergent   int
	Fabricsof   int
}

func (mo *OrdersGorm) GetOrderByUser(userid uint) ([]ListOrderUser, error) {
	order := []ListOrderUser{}
	rows, err := mo.db.Table("orders").
		Select("orders.id , stores.storename, orders.status, stores.filename ").
		Joins("join stores on stores.id = orders.storeid").
		Where("orders.userid = ? AND orders.deleted_at IS NULL ", userid).
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		report := ListOrderUser{}
		err := rows.Scan(&report.Ordersid, &report.Storename, &report.Status, &report.Filename)
		if err != nil {
			fmt.Println(err)
		}
		order = append(order, report)
	}
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (mo *OrdersGorm) GetOrderDetailByID(orderid uint) (*ListOrderUser, error) {
	rows, err := mo.db.Table("orders").
		Select("orders.id ,stores.storename,orders.status,stores.filename ,partners.id,partners.partnername,partners.tall ,orders.price ,orders.dry ,orders.detergent,orders.fabricsof ").
		Joins("join stores on stores.id = orders.storeid").
		Joins("join partners on partners.id =orders.partnerid ").
		Where("orders.id = ?", orderid).
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	report := ListOrderUser{}
	for rows.Next() {

		err := rows.Scan(&report.Ordersid, &report.Storename, &report.Status, &report.Filename, &report.Partnerid, &report.Partnername, &report.Tall, &report.Price, &report.Dry, &report.Detergent, &report.Fabricsof)
		if err != nil {
			fmt.Println(err)
		}
	}
	if err != nil {
		return nil, err
	}
	return &report, nil
	

}

func (mo *OrdersGorm) ListHistoryAll(userid uint) ([]ListOrderUser, error) {
	order := []ListOrderUser{}
	rows, err := mo.db.Table("orders").
		Select("orders.id , stores.storename, orders.status, stores.filename").
		Joins("join stores on stores.id = orders.storeid").
		Where("orders.userid = ? AND orders.deleted_at IS NULL AND status=3 ", userid).
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		report := ListOrderUser{}
		err := rows.Scan(&report.Ordersid, &report.Storename, &report.Status, &report.Filename)
		if err != nil {
			fmt.Println(err)
		}
		order = append(order, report)
	}
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (mo *OrdersGorm) GetOrderActive() ([]OrderDetailByUser, error) {
	order := []OrderDetailByUser{}
	rows, err := mo.db.Table("orders").
		Select("orders.id, orders.userid, orders.storeid ,orders.partnerid ,orders.price ,orders.dry ,orders.detergent,orders.fabricsof ,orders.status,stores.storename,stores.filename").
		Joins("join stores on stores.id = orders.storeid").
		Where("status= 1  AND orders.deleted_at IS NULL").Rows()
	if err != nil {
		fmt.Println(err)
	}

	for rows.Next() {
		report := OrderDetailByUser{}
		err := rows.Scan(&report.Ordersid, &report.Userid, &report.Storeid, &report.Partnerid, &report.Price, &report.Dry, &report.Detergent, &report.Fabricsof, &report.Status, &report.Storename, &report.Filename)
		if err != nil {
			fmt.Println(err)
		}
		order = append(order, report)
	}
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (mo *OrdersGorm) UpdateStatusTwo(order *Orders) error {
	id := order.Partnerid
	fmt.Printf("test min %v:", id)
	return mo.db.Model(&Orders{}).Where("id = ?", order.ID).Updates(map[string]interface{}{"partnerid": id, "status": 2}).Error
}

func (mo *OrdersGorm) GetOrderByPartner(partnerid uint) ([]ListOrderUser, error) {
	order := []ListOrderUser{}
	rows, err := mo.db.Table("orders").
		Select("orders.id , stores.storename, orders.status, stores.filename").
		Joins("join stores on stores.id = orders.storeid").
		Where("partnerid = ? AND status= ? AND orders.deleted_at IS NULL", partnerid, 2).
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		report := ListOrderUser{}
		err := rows.Scan(&report.Ordersid, &report.Storename, &report.Status, &report.Filename)
		if err != nil {
			fmt.Println(err)
		}
		order = append(order, report)
	}
	if err != nil {
		return nil, err
	}
	return order, nil
}

type DetailOrderWith struct {
	Ordersid  uint
	Userid    uint
	Storeid   uint
	Partnerid uint
	Price     float32
	Dry       int
	Detergent int
	Fabricsof int
	Status    int
	Storename string
	Filename  string
	Phone     string
	Username  string
	Address   string
	Latitude  float32
	Longitude float32
}

func (mo *OrdersGorm) GetPartnerOrder(orderid uint) (*DetailOrderWith, error) {
	rows, err := mo.db.Table("orders").
		Select("orders.id, orders.userid, orders.storeid ,orders.partnerid ,orders.price ,orders.dry ,orders.detergent,orders.fabricsof ,orders.status,stores.storename,stores.filename,users.phone,users.username,users.address").
		Joins("join stores on stores.id = orders.storeid").
		Joins("join users on users.id = orders.userid").
		Where("orders.id = ?  AND orders.deleted_at IS NULL", orderid).Rows()
	if err != nil {
		fmt.Println(err)
	}
	report := DetailOrderWith{}
	for rows.Next() {
		err := rows.Scan(&report.Ordersid, &report.Userid, &report.Storeid, &report.Partnerid, &report.Price, &report.Dry, &report.Detergent, &report.Fabricsof, &report.Status, &report.Storename, &report.Filename, &report.Phone, &report.Username, &report.Address)
		if err != nil {
			fmt.Println(err)
		}
	}
	if err != nil {
		return nil, err
	}
	fmt.Printf("tTtttfttft %v", report)
	return &report, nil
}

func (mo *OrdersGorm) ListPartnerHistoryAll(id uint) ([]ListOrderUser, error) {
	order := []ListOrderUser{}
	rows, err := mo.db.Table("orders").
		Select("orders.id , stores.storename, orders.status, stores.filename").
		Joins("join stores on stores.id = orders.storeid").
		Where("status = ? AND partnerid = ? AND orders.deleted_at IS NULL", 3, id).
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		report := ListOrderUser{}
		err := rows.Scan(&report.Ordersid, &report.Storename, &report.Status, &report.Filename)
		if err != nil {
			fmt.Println(err)
		}
		order = append(order, report)
	}
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (mo *OrdersGorm) GetorderPartnerByID(orderid uint) (*Orders, error) {
	order := Orders{}
	if err := mo.db.Where("id=?", orderid).First(&order).Error; err != nil {
		return nil, err
	}
	return &order, nil
}

func (mo *OrdersGorm) GetorderDetailPartnerByID(orderid uint) (*DetailOrderWith, error) {
	rows, err := mo.db.Table("orders").
		Select("orders.id, orders.userid, orders.storeid ,orders.partnerid ,orders.price ,orders.dry ,orders.detergent,orders.fabricsof ,orders.status,stores.storename,stores.filename").
		Joins("join stores on stores.id = orders.storeid").
		Where("orders.id = ?  AND orders.deleted_at IS NULL", orderid).Rows()
	if err != nil {
		fmt.Println(err)
	}
	report := DetailOrderWith{}
	for rows.Next() {
		err := rows.Scan(&report.Ordersid, &report.Userid, &report.Storeid, &report.Partnerid, &report.Price, &report.Dry, &report.Detergent, &report.Fabricsof, &report.Status, &report.Storename, &report.Filename)
		if err != nil {
			fmt.Println(err)
		}
	}
	if err != nil {
		return nil, err
	}
	return &report, nil
}

func (mo *OrdersGorm) UpdateStatusFinish(orderid uint) error {

	return mo.db.Model(&Orders{}).Where("id = ?", orderid).Update("status", 3).Error
}
