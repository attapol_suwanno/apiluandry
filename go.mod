module apilaundry

go 1.14

require (
	github.com/floresj/go-contrib-mobile v0.0.0-20150714204826-47557cfa26f5
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/static v0.0.0-20191128031702-f81c604d8ac2
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.12
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	golang.org/x/mobile v0.0.0-20200329125638-4c31acba0007 // indirect
	honnef.co/go/tools v0.0.1-2020.1.4
)
