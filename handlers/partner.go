package handlers

import (
	"apilaundry/models"
	"fmt"

	"github.com/gin-gonic/gin"
)

type PartnerModel struct {
	ID          uint
	Partnername string `gorm:"not null" json:"partnername`
	Email       string `gorm:"unique_index;not null" json:"email"`
	Password    string `gorm:"not null" json:"password"`
	Token       string `gorm:"unique_index"`
	Tall        string
	Filename    string
}

type Partnerhandler struct {
	mp models.Partnerservice
}

func NewPartnerhandlers(mp models.Partnerservice) *Partnerhandler {
	return &Partnerhandler{mp}
}

func (hp *Partnerhandler) PartnerLogin(c *gin.Context) {
	var login = new(models.Partner)
	if err := c.BindJSON(login); err != nil {
		c.Status(400)
		return
	}
	fmt.Printf("login >>> :%v", login)
	partner := new(models.Partner)
	partner.Email = login.Email
	partner.Password = login.Password
	token, err := hp.mp.PartnerLoginpost(partner)
	if err != nil {
		c.JSON(401, gin.H{
			"massage": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token": token,
	})
}

func (hp *Partnerhandler) PartnerCreateSignup(c *gin.Context) {
	var req = new(PartnerModel)
	if err := c.BindJSON(req); err != nil {
		c.Status(400)
		return
	}
	fmt.Printf("register >>> :%v", req)
	partner := new(models.Partner)
	partner.Email = req.Email
	partner.Password = req.Password
	partner.Tall = req.Tall
	partner.Partnername = req.Partnername
	if err := hp.mp.PartnerSignup(partner); err != nil {
		c.JSON(401, gin.H{
			"massage": "User already exits",
		})
		return
	}
	c.JSON(201, gin.H{
		"token": partner.Token,
	})
}

func (hp *Partnerhandler) LogoutPartner(c *gin.Context) {
	partnerLogin := c.Value("partner").(*models.Partner)
	fmt.Printf("partner logout : %v", partnerLogin)

	if err := hp.mp.UserPartner(uint(partnerLogin.ID)); err != nil {
		c.JSON(500, gin.H{
			"massage": err.Error(),
		})
		return
	}
	c.Status(200)
}
