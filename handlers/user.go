package handlers

import (
	"apilaundry/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UserModel struct {
	ID       uint
	Username string `gorm:"not null" json:"name`
	Email    string `gorm:"unique_index;not null" json:"email"`
	Password string `gorm:"not null" json:"password"`
	Token    string `gorm:"unique_index"`
	Phone    string
	Dob      string
	Address  string
	Filename string `gorm:"not null "`
}

type Userhandler struct {
	mu models.Userservice
}

func NewUserhandlers(mu models.Userservice) *Userhandler {
	return &Userhandler{mu}
}

func (hu *Userhandler) UserLogin(c *gin.Context) {
	var login = new(models.User)
	if err := c.BindJSON(login); err != nil {
		c.Status(400)
		return
	}
	fmt.Printf("login >>> :%v", login)
	user := new(models.User)
	user.Email = login.Email
	user.Password = login.Password
	token, err := hu.mu.UserLoginpost(user)
	if err != nil {
		c.JSON(401, gin.H{
			"massage": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token": token,
	})
}

func (hu *Userhandler) UserCreateSignup(c *gin.Context) {
	var req = new(UserModel)
	if err := c.BindJSON(req); err != nil {
		c.Status(400)
		return
	}
	fmt.Printf("register >>> :%v", req)
	user := new(models.User)
	user.Email = req.Email
	user.Password = req.Password
	user.Username = req.Username
	user.Phone = req.Phone
	user.Address = req.Address
	user.Dob = req.Dob
	if err := hu.mu.UserSignup(user); err != nil {
		c.JSON(401, gin.H{
			"massage": "User already exits",
		})
		return
	}
	c.JSON(201, gin.H{
		"token": user.Token,
	})
}

func (hu *Userhandler) Logout(c *gin.Context) {
	userLogin := c.Value("user").(*models.User)
	fmt.Printf("user logout : %v", userLogin)

	if err := hu.mu.UserLogOut(userLogin.ID); err != nil {
		c.JSON(500, gin.H{
			"massage": err.Error(),
		})
		return
	}
	c.Status(200)
}

type Profile struct {
	Name    string
	Picture string
}

func (hu *Userhandler) ProfileByUser(c *gin.Context) {
	userLogin := c.Value("user").(*models.User)
	fmt.Printf("get id User%v", userLogin)

	found, err := hu.mu.GetProfile(uint(userLogin.ID))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"Token":    found.Filename,
		"Address":  found.Address,
		"Phone":    found.Phone,
		"Username": found.Username,
		"Email":    found.Email,
		"Filename": found.Filename,
	})
}
