package handlers

import (
	"apilaundry/models"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateOrders struct {
	ID        uint
	Userid    uint
	Storeid   uint
	Partnerid uint
	Price     float32
	Dry       int
	Detergent int
	Fabricsof int
	Status    int
}

type ReportModel struct {
	Ordersid  uint
	Userid    uint
	Storeid   uint
	Partnerid uint
	Price     float32
	Dry       int
	Detergent int
	Fabricsof int
	Status    int
	Storename string
	Filename  string
}

type Ordershandler struct {
	mo models.Ordersservice
}

func NewOrdershandlers(mo models.Ordersservice) *Ordershandler {
	return &Ordershandler{mo}
}

func (ho *Ordershandler) CreateOrder(c *gin.Context) {
	data := new(CreateOrders)
	if err := c.BindJSON(&data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("testdata %v", data)

	userLogin := c.Value("user").(*models.User)
	fmt.Printf("get id User%v", userLogin)

	ordertable := new(models.Orders)
	ordertable.ID = data.ID
	ordertable.Userid = uint(userLogin.ID)
	ordertable.Storeid = data.Storeid
	ordertable.Partnerid = data.Partnerid
	ordertable.Price = data.Price
	ordertable.Dry = data.Dry
	ordertable.Detergent = data.Detergent
	ordertable.Fabricsof = data.Fabricsof
	ordertable.Status = 1
	if err := ho.mo.CreateOrdertable(ordertable); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
	fmt.Printf("testOrder %v", ordertable)
	c.JSON(201, gin.H{
		"id": ordertable.ID,
	})
}

func (ho *Ordershandler) ShowDetailOrder(c *gin.Context) {
	idstore := c.Param("order_id")
	id, err := strconv.Atoi(idstore)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("order id %v", id)

	found, err := ho.mo.GetOrderByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, found)
}

func (ho *Ordershandler) DeleteOrderUser(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("jhhhhhhhhhh")
	if err := ho.mo.DeleteOrder(uint(id)); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

func (ho *Ordershandler) OrderByUser(c *gin.Context) {
	userLogin := c.Value("user").(*models.User)
	fmt.Printf("get id User%v", userLogin)

	found, err := ho.mo.GetOrderByUser(uint(userLogin.ID))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, found)
}

func (ho *Ordershandler) ShowHistory(c *gin.Context) {
	userLogin := c.Value("user").(*models.User)
	fmt.Printf("get id User%v", userLogin)

	data, err := ho.mo.ListHistoryAll(uint(userLogin.ID))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, data)
}

func (ho *Ordershandler) ShowOrderinPartner(c *gin.Context) {
	data, err := ho.mo.GetOrderActive()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	order := []models.OrderDetailByUser{}
	for _, a := range data {
		order = append(order, models.OrderDetailByUser{
			Ordersid:  a.Ordersid,
			Storeid:   a.Storeid,
			Status:    a.Status,
			Price:     a.Price,
			Storename: a.Storename,
			Filename:  a.Filename,
			Dry:       a.Dry,
			Fabricsof: a.Fabricsof,
			Detergent: a.Detergent,
		})
	}
	c.JSON(http.StatusOK, order)
	fmt.Printf("bbbbbbb %v", order)

}

func (ho *Ordershandler) ShowDetailOrderinPartner(c *gin.Context) {
	idstore := c.Param("order_id")
	id, err := strconv.Atoi(idstore)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("data get store_id %v", id)

	found, err := ho.mo.GetorderDetailPartnerByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, found)
}

func (ho *Ordershandler) UpdateStatusStepTwo(c *gin.Context) {
	idStr := c.Param("order_id")
	fmt.Printf("eeeee ----> id %v", idStr)
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	orderid := uint(id)

	partnerLogin := c.Value("partner").(*models.Partner)
	fmt.Printf("Check data partner : %v", partnerLogin)

	found, err := ho.mo.GetorderPartnerByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Println("found: ", found)
	found.Partnerid = partnerLogin.ID
	found.ID = orderid
	if err := ho.mo.UpdateStatusTwo(found); err != nil {
		c.JSON(500, gin.H{
			"maessage": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"message": "success",
	})
}

func (ho *Ordershandler) OrderByPartner(c *gin.Context) {
	partnerLogin := c.Value("partner").(*models.Partner)
	fmt.Printf("Check data partner : %v", partnerLogin)
	found, err := ho.mo.GetOrderByPartner(uint(partnerLogin.ID))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, found)
}

func (ho *Ordershandler) OrderPartnerByOrderID(c *gin.Context) {
	idpartner := c.Param("order_id")
	id, err := strconv.Atoi(idpartner)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("order id %v", id)

	found, err := ho.mo.GetPartnerOrder(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, found)
}

func (ho *Ordershandler) ShowHistoryPartner(c *gin.Context) {
	partnerLogin := c.Value("partner").(*models.Partner)
	fmt.Printf("Check data partner : %v", partnerLogin)
	data, err := ho.mo.ListPartnerHistoryAll(partnerLogin.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	order := []models.ListOrderUser{}
	for _, a := range data {
		order = append(order, models.ListOrderUser{
			Ordersid:  a.Ordersid,
			Storename: a.Storename,
			Status:    a.Status,
			Filename:  a.Filename,
		})
	}
	c.JSON(http.StatusOK, order)

}

func (ho *Ordershandler) UpdateStatusStepFinish(c *gin.Context) {
	idStr := c.Param("order_id")
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	orderid := uint(id)
	fmt.Printf("id %v", orderid)

	order := new(models.Orders)
	if err := c.BindJSON(order); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("alubum %v", order)

	found, err := ho.mo.GetorderPartnerByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	found.Partnerid = order.ID
	found.ID = order.ID
	if err := ho.mo.UpdateStatusFinish(orderid); err != nil {
		c.JSON(500, gin.H{
			"maessage": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"message": "success",
	})

}
