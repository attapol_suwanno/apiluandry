package handlers

import (
	"apilaundry/models"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Storehandler struct {
	ms models.Storeservice
}

func NewStorehandlers(ms models.Storeservice) *Storehandler {
	return &Storehandler{ms}
}

func (hs *Storehandler) ListStore(c *gin.Context) {
	searchInput, _ := c.GetQuery("search_input")
	data, err := hs.ms.ListStoreAll(searchInput)

	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	stores := []models.StoreModel{}
	for _, a := range data {
		stores = append(stores, models.StoreModel{
			ID:             uint(a.ID),
			Storename:      a.Storename,
			Prices:         a.Prices,
			Location:       a.Location,
			Dry:            a.Dry,
			Fabricsof:      a.Fabricsof,
			Detergent:      a.Detergent,
			Promotion:      a.Promotion,
			PromotionPrice: a.PromotionPrice,
			Filename:       a.Filename,
		})
	}
	c.JSON(http.StatusOK, stores)
}

func (hs *Storehandler) ListStoreByIdStore(c *gin.Context) {
	idstore := c.Param("store_id")
	id, err := strconv.Atoi(idstore)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("data get id %v", id)

	found, err := hs.ms.GetStoreID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, found)

}

func (hs *Storehandler) GetlocationByStoreID(c *gin.Context) {
	idstore := c.Param("store_id")
	id, err := strconv.Atoi(idstore)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("data get id %v", id)
	found, err := hs.ms.GetLocation(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, found)

}

func (hs *Storehandler) Search(c *gin.Context) {
	search := c.Query("search")
	id, err := strconv.Atoi(search)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("data get id %v", id)
	found, err := hs.ms.GetNameStore(search)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, found)

}
