package main

import (
	"apilaundry/config"
	"apilaundry/handlers"
	"apilaundry/models"
	"apilaundry/mw"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	conf := config.Load()
	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.LogMode(true) // dev only!!!!
	if err := db.AutoMigrate(&models.Store{}, &models.Price{}, &models.Location{}, &models.Orders{}, &models.User{}, &models.Partner{}).Error; err != nil {
		log.Fatal(err)
	}
	r := gin.Default()

	mu := models.NewUserService(db, conf.HMACKey)
	ms := models.NewStoreService(db)
	mp := models.NewPartnerService(db, conf.HMACKey)
	mo := models.NewOrdersService(db)

	hu := handlers.NewUserhandlers(mu)
	hs := handlers.NewStorehandlers(ms)
	hp := handlers.NewPartnerhandlers(mp)
	ho := handlers.NewOrdershandlers(mo)

	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:3000"}
	config.AllowHeaders = []string{"Authorization", "Content-Type"}

	r.Use(cors.New(config))
	r.POST("/login", hu.UserLogin)
	r.POST("/singup", hu.UserCreateSignup)

	r.POST("/loginpartner", hp.PartnerLogin)
	r.POST("/PartnerCreateSignup", hp.PartnerCreateSignup)

	r.GET("/stores", hs.ListStore)
	r.GET("/store/:store_id", hs.ListStoreByIdStore)
	r.GET("/location/:store_id", hs.GetlocationByStoreID)
	r.GET("/searchname/:search", hs.Search)
	r.Static("/images", "./upload")

	

	auth := r.Group("/auth")
	auth.Use(mw.AuthRequired(mu))
	{
		auth.POST("/order", ho.CreateOrder)

		auth.GET("/orders/user_id", ho.OrderByUser)

		auth.DELETE("/orders/:id", ho.DeleteOrderUser)

		auth.GET("/orderd/:order_id", ho.ShowDetailOrder)

		auth.GET("/history", ho.ShowHistory)

		auth.GET("/profile", hu.ProfileByUser)

		auth.POST("/logout", hu.Logout)
	}

	authp := r.Group("/authp")
	authp.Use(mw.AuthRequiredPartner(mp))
	{
		authp.PATCH("/orderupdate/:order_id", ho.UpdateStatusStepTwo)

		authp.GET("/orderallpartnertlist", ho.OrderByPartner)

		authp.GET("/orderallparner", ho.ShowOrderinPartner)

		authp.GET("/orderde/:order_id", ho.ShowDetailOrderinPartner)

		authp.GET("/orderdetail/:order_id", ho.OrderPartnerByOrderID)

		authp.PATCH("/orderupdateFinish/:order_id", ho.UpdateStatusStepFinish)

		authp.GET("/historyPartner", ho.ShowHistoryPartner)

		authp.POST("/logoutPartner", hp.LogoutPartner)
	}

	r.Run(":8080")

}
